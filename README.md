We welcome your interest in our Intravital Microscopy Artifact Reduction Tool software (IMART).

You will find a pdf version of the text below as an attachment to this email, please read this carefully.

In using IMART, you acknowledge that you have read, understood and agree to be bound by the following terms and conditions.

You can download IMART by running 'sh download-imart.sh'


Once you download the zip file, unzip it and look at the file "readme-imart.pdf" for installation instructions.

By installing and using IMART on your computer you acknowledge that we will not be held responsible for any damages to your hardware or loss of data that may occur when you install and use IMART. The use of IMART is undertaken at your own risk.

In addition, regarding your source code request, we do not provide the source code of IMART.


Copyright
All contents of IMART, including the software, design, text, images and graphics are protected by the copyright laws of the United States of America. You may not reproduce, transmit, adapt, distribute, sell, modify or publish or otherwise use any of the material associated with IMART without our prior written consent. IMART is copyrighted by the Board of Trustees of Purdue University.

Privacy Statement
We do not collect any information from or about you in using IMART.

Acknowledgement
Publications and presentations including results obtained using the IMART software must acknowledge the use of IMART, stipulating  that

"IMART was obtained from the Indiana Obrien Center for Advanced Microscopic Analysis, which is supported by funding 
from the National Institutes of Health NIH/NIDDK P50 DK 61594."

We also ask that you cite the following paper in your manuscript:

Dunn KW, Lorenz KS, Salama P, Delp EJ. IMART software for correction of motion artifacts in images collected in intravital microscopy. IntraVital 2014; 3:e28210; http://dx.doi.org/10.4161/intv.28210



How to Contact Us

IMART is distributed "as is" with no implied or expressed warranty.

We are very interested in hearing from users.

If you have any concerns or questions, please use the contact details found in the IMART software or by sending an email to imart@ecn.purdue.edu

Thank You,

The IMART Team
