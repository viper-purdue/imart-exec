function img_filtered = space_filtering(img)

img_filtered = imfilter(img, fspecial('disk',1),'replicate');

% h = [0.1 0.1 0.1;
%      0.1 0.2 0.1;
%      0.1 0.1 0.1];
 
% Assume zero-padded boundary
% img_filtered = filter2(h,img);

% Assume circular boundary
% [M N] = size(img);

%Initialize output image
% img_filtered = zeros(M,N);
% 
% for m = 1:M
%     for n = 1:N
%         r = m-1:m+1;
%         s = n-1:n+1;
% 
%         r_ind = (r<1);
%         r(r_ind) = M - r(r_ind);
%         s_ind = (s<1);
%         s(s_ind) = N - s(s_ind);
% 
%         r_ind = (r>M);
%         r(r_ind) = r(r_ind) - M;
%         s_ind = (s>M);
%         s(s_ind) = s(s_ind) - N;
% 
%         img_filtered(m,n) = sum(sum(img(r,s) .* h));
%     end
% end
