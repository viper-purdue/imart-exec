function [cor_table, max_cor] = reduce_registration(cor_ind, method)
% Transforms a table of incremental registration offsets into a table of
% absolute registration offsets

N = size(cor_ind,1);

if (strcmpi(method, 'moving'))
    cor_table = cor_ind - repmat(cor_ind(1,:),N,1);
    for n = 1:N
        cor_table(n,3) = sum(cor_table(1:n,1));
        cor_table(n,4) = sum(cor_table(1:n,2));
    end
    min_cor = min(cor_table(:,3:4));
    cor_table(:,3:4) = cor_table(:,3:4) - repmat(min_cor,N,1);
    max_cor = ceil(max(cor_table(:,3:4)));

elseif (strcmpi(method, 'static'))
    cor_table = cor_ind - repmat(cor_ind(1,:),N,1);
    cor_table(:,3:4) = cor_table(:,1:2);
    
    min_cor = min(cor_table(:,3:4));
    cor_table(:,3:4) = cor_table(:,3:4) - repmat(min_cor,N,1);
    max_cor = ceil(max(cor_table(:,3:4)));
end