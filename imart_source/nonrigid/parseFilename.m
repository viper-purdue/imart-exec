function [fileIn, lenNum, N_start, N_end] = parseFilename(fileBegin, fileEnd)

% Parse filenames
[directory_in, name, fileIn.ext] = fileparts(fileBegin);
[~, name2]                       = fileparts(fileEnd);
fileIn.directory                 = [directory_in filesep];

% Find ending index of number sequence
ind = length(name);
ch  = name(ind);
while (isnan(str2double(ch)) || ~isreal(str2double(ch)))
    ind = ind - 1;
    ch = name(ind);
end
indEnd = ind;
% Find beginning index of number sequence
while ~isnan(str2double(ch))
    ind = ind - 1;
    ch = name(ind);
end
indBegin      = ind;
fileIn.name   = name(1:indBegin);
fileIn.append = name(indEnd+1:end);
N_start       = str2double(name(indBegin+1:indEnd));
N_end         = str2double(name2(indBegin+1:indEnd));
lenNum        = indEnd - indBegin;
% fileOut.name  = fileIn.name;


% file      = find_name(fileIn, N_start, lenNum);
% imginfo   = imfinfo(file);
% imgHeight = imginfo.Height;
% imgWidth  = imginfo.Width;
% imgDepth  = imginfo.SamplesPerPixel;
% 
% numFrames = N_end-N_start+1;
