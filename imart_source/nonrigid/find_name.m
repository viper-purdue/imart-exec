function file = find_name(fileStruct, n, len)

% if (nargin==4)
%     append = [];
% elseif (nargin<4 || nargin>5)
%     disp('Incorrect number of arguments');
%     return;
% end

% switch len
%     case 1
%         file = [fileStruct.directory fileStruct.name num2str(n) fileStruct.append fileStruct.ext];
%     case 2
%         if (n<10)
%             file = [fileStruct.directory fileStruct.name '0'   num2str(n) fileStruct.append fileStruct.ext];
%         else
%             file = [fileStruct.directory fileStruct.name       num2str(n) fileStruct.append fileStruct.ext];
%         end
%     case 3
%         if (n<10)
%             file = [fileStruct.directory fileStruct.name '00'  num2str(n) fileStruct.append fileStruct.ext];
%         elseif (n<100)
%             file = [fileStruct.directory fileStruct.name '0'   num2str(n) fileStruct.append fileStruct.ext];
%         else
%             file = [fileStruct.directory fileStruct.name       num2str(n) fileStruct.append fileStruct.ext];
%         end
%     case 4
%         if (n<10)
%             file = [fileStruct.directory fileStruct.name '000' num2str(n) fileStruct.append fileStruct.ext];
%         elseif (n<100)
%             file = [fileStruct.directory fileStruct.name '00'  num2str(n) fileStruct.append fileStruct.ext];
%         elseif (n<1000)
%             file = [fileStruct.directory fileStruct.name '0'   num2str(n) fileStruct.append fileStruct.ext];
%         else
%             file = [fileStruct.directory fileStruct.name       num2str(n) fileStruct.append fileStruct.ext];
%         end
% end

fileStr = [fileStruct.name '%0' num2str(len) 'd' fileStruct.append fileStruct.ext];
file = [fileStruct.directory sprintf(fileStr, n)];

end
