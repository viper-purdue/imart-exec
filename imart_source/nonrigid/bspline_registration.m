function [Icor, Igrid, O_trans] = bspline_registration(img_static, img_moving, spacing, options, comp, opt_mode, h_waitbar)

% Read images
if(exist('comp','var'))
    I1 = extract_component(img_moving, comp);
    I2 = extract_component(img_static, comp);
else
    I1 = img_moving;
    I2 = img_static;
end

% b-spline grid spacing in x and y direction
spacing = [spacing spacing];

% Make the initial b-spline registration grid
O_trans = make_init_grid(spacing,size(I1));

% Convert all values to type double
I1 = double(I1);
I2 = double(I2);
O_trans = double(O_trans); 

% Smooth both images for faster registration
% I1s = imfilter(I1,fspecial('gaussian'))+0.001;
% I2s = imfilter(I2,fspecial('gaussian'))+0.001;

% Optimizer parameters
if (strcmpi(opt_mode,'lbfgs'))
    optim = struct('Display','off','GradObj','on','HessUpdate','lbfgs','MaxIter',100,'DiffMinChange',0.01,'DiffMaxChange',0.25,'TolFun',1e-14,'StoreN',5,'GoalsExactAchieve',0);
elseif (strcmpi(opt_mode,'lm'))
    optim = optimset('MaxIter',100,'GradObj','on','GradConstr','on','DiffMinChange',0.01,'DiffMaxChange',0.25,'TolFun',1e-10);
% optim = optimset('GradObj','on','LargeScale','off','Display','iter','MaxIter',40,'DiffMinChange',0.01,'DiffMaxChange',1);
end
% options.MaxIter = optim.MaxIter;

% Reshape O_trans from a matrix to a vector
sizes   = size(O_trans);
O_trans = O_trans(:);

if (exist('h_waitbar','var'))
    if (strcmpi(opt_mode,'lbfgs'))
        O_trans = fminlbfgs(@(x)bspline_registration_gradient(x,sizes,spacing,I1,I2,options,[],[],[],[],[],h_waitbar),O_trans,optim);
    elseif (strcmpi(opt_mode,'lm'))
        O_trans = lsqnonlin(@(x)bspline_registration_gradient(x,sizes,spacing,I1,I2,options,[],[],[],[],[],h_waitbar),O_trans,[],[],optim);
    end
else
    % Start the b-spline nonrigid registration optimizer
    if (strcmpi(opt_mode,'lbfgs'))
        O_trans = fminlbfgs(@(x)bspline_registration_gradient(x,sizes,spacing,I1,I2,options),O_trans,optim);
    elseif (strcmpi(opt_mode,'lm'))
        O_trans = lsqnonlin(@(x)bspline_registration_gradient(x,sizes,spacing,I1,I2,options),O_trans,[],[],optim);
    end
end
% O_trans = lsqnonlin(@(x)bspline_registration_image(x,sizes,spacing,I1s,I2s,options.type),O_trans,[],[],optim);
% O_trans = fminunc(@(x)bspline_registration_gradient(x,sizes,spacing,I1,I2,options),O_trans,optim);


% Reshape O_trans from a vector to a matrix
O_trans = reshape(O_trans,sizes);

% Transform the input image with the found optimal grid
Icor = bspline_transform(O_trans,img_moving,spacing,3); 
% [Icor,O_trans,spacing,M,Bx,By,Fx,Fy] = register_images(I1,I2,options);

% Make a transformed grid image
Igrid = make_grid_image(spacing,size(I1));
Igrid = bspline_transform(O_trans,Igrid,spacing,3);


% Show the registration results
% figure;
% subplot(2,2,1); imshow(I1);    title('Warped Image');
% subplot(2,2,2); imshow(I2);    title('Static Image');
% subplot(2,2,3); imshow(Icor);  title('Registered Image');
% subplot(2,2,4); imshow(Igrid); title('Deformation Grid');
