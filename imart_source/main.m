function [] = main(fileBegin, fileEnd, fileOut, flags, numBits)

% Error checking
if (flags.perform.spaceFilter == false)
    flags.output.spaceFilter = false;
end
if (flags.perform.rigidRegister == false)
    flags.output.rigidRegister = false;
end
if (flags.perform.nonRigidRegister == false)
    flags.output.nonRigidRegister = false;
    flags.output.nonRigidRegisterGrid = false;
end
if (flags.perform.timeFilter == false)
    flags.output.timeFilter = false;
end
if (flags.perform.threshold == false)
    flags.output.threshold = false;
end
if (flags.perform.morphFilter == false)
    flags.output.morphFilter = false;
end
if (flags.perform.threshold == false)
    flags.output.threshold = false;
end
if (flags.perform.binaryMask == false)
    flags.output.binaryMask = false;
end


addpath('./nonrigid/');


% Parse filenames
[fileIn, lenNum, N_start, N_end] = parseFilename(fileBegin, fileEnd);
fileOut.name  = fileIn.name;


% Gather image information
file      = find_name(fileIn, N_start, lenNum);
imginfo   = imfinfo(file);
imgHeight = imginfo.Height;
imgWidth  = imginfo.Width;
%if (strcmp(imginfo.ColorType, 'truecolor'))
%    imgDepth = 3;
%elseif (strcmp(imginfo.ColorType, 'grayscale'))
%    imgDepth = 1;
%end
imgDepth  = imginfo.SamplesPerPixel;
imgBits   = min(imginfo.BitsPerSample);
imgFormat = class(imread(find_name(fileIn, N_start, lenNum)));

if ~exist('numBits','var') || isempty(numBits)
    numBits = imgBits;
end
assert(numBits <= imgBits, ...
    'Error: Number of bits specified is greater than actual number of bits.');

numFrames = N_end-N_start+1;


%% Find maximum displacement to determine output image size after rigid registration
if (flags.perform.rigidRegister)
    cor_ind   = zeros(numFrames, 2);
    cor_ind(1,:) = [0 0];
    
    if (matlabpool('size')==0)
        matlabpool open;
    end
    parfor n = N_start+1:N_end
        
        % Read current moving image
        filename_moving = find_name(fileIn, n, lenNum);
        if (imgDepth == 3)
            img_moving = extract_component(im2double(imread(filename_moving)), flags.rigidRegister.comp);
        elseif (imgDepth == 1)
            img_moving = im2double(imread(filename_moving));
        end
        img_moving = img_moving.*2^(imgBits-numBits);
        
        % Read current reference image
        if (strcmpi(flags.rigidRegister.reference, 'moving'))
            filename_static = find_name(fileIn, n-flags.rigidRegister.referenceNum, lenNum);
        elseif (strcmpi(flags.rigidRegister.reference, 'static'))
            filename_static = find_name(fileIn, flags.rigidRegister.referenceNum, lenNum);
        end
        if (imgDepth == 3)
            img_static = extract_component(im2double(imread(filename_static)), flags.rigidRegister.comp);
        elseif (imgDepth == 1)
            img_static = im2double(imread(filename_static));
        end
        img_static = img_static.*2^(imgBits-numBits);
        
        % Smooth both images for faster registration
        if (flags.rigidRegister.blur)
            img_static_temp = space_filtering(img_static);
            img_moving_temp = space_filtering(img_moving);
        else
            img_static_temp = img_static;
            img_moving_temp = img_moving;
        end
        
        % Register images
        % x = lsqnonlin(@(x)affine_registration_image(x,img_moving_temp,img_static_temp,affine_type),[0 0 0],[],[]);%,optimset('MaxIter',100,'TolFun',1e-10));
        x = fminunc(@(x)affine_registration_image(x,img_moving_temp,img_static_temp,flags.rigidRegister.metric),[0 0 0]);%,[],[]);%,optimset('MaxIter',100,'TolFun',1e-10));
        % x = fminsearch(@(x)affine_registration_image(x,img_moving_temp,img_static_temp,affine_type),[0 0 0]);%,[],[]);%,optimset('MaxIter',100,'TolFun',1e-10));
        % cor_ind(n-N_start+1,:) = [-x(1) -x(2)];
        cor_ind(n,:) = [-x(1) -x(2)];
        
        % Make the affine transformation matrix
        % M = make_transformation_matrix(x(1:2),x(3));
        
        % img_reg = affine_transform(img_moving,M,3);
        
        % Show the registration results
        % figure;
        % subplot(1,3,1), imshow(img_moving);
        % subplot(1,3,2), imshow(img_static);
        % subplot(1,3,3), imshow(img_reg);
        
    end
    if (matlabpool('size')>0)
        matlabpool close;
    end
    
    cor_ind = [0 0; cor_ind(N_start+1:N_end,:)];
    % cor_ind = round(cor_ind);
    % [height width depth] = size(img_static);
    [cor_table max_cor] = reduce_registration(cor_ind, flags.rigidRegister.reference);
    clear cor_ind;
    
    if (~isdir(fileOut.directory))
        mkdir(fileOut.directory);
    end
    save([fileOut.directory 'rigidRegistration_table.mat'], 'cor_table', 'max_cor');

else
    if exist([fileOut.directory 'rigidRegistration_table.mat'], 'file')
        load([fileOut.directory 'rigidRegistration_table.mat'], 'cor_table', 'max_cor');
    else
        cor_table = zeros(numFrames,4);
        max_cor   = [0 0];
    end
end


%% Perform non-rigid registration
if (flags.perform.nonRigidRegister)
    
    % Pre-allocate
    O_trans_total    = cell(numFrames,1);
    O_trans_total{1} = make_init_grid([flags.nonRigidRegister.spacing flags.nonRigidRegister.spacing], [imgHeight+max_cor(1) imgWidth+max_cor(2)]);
    
    % Assign reference image for non-rigid registration
    filename_static = find_name(fileIn, N_start, lenNum);
    img_static = im2double(imread(filename_static));
    img_static = img_static.*2^(imgBits-numBits);
    
    if (flags.perform.spaceFilter)
        img_static = space_filtering(img_static);
    end
    % img_static_reg = zeros(imgHeight+max_cor(1), imgWidth+max_cor(2), imgDepth);
    % img_static_reg(1+cor_table(1,3):imgHeight+cor_table(1,3),1+cor_table(1,4):imgWidth+cor_table(1,4), :) = img_static;
    t = maketform('affine',[1 0 ; 0 1; cor_table(1,4) cor_table(1,3)]);
    bounds = [1 1; imgWidth+max_cor(2) imgHeight+max_cor(1)];
    img_static_reg = imtransform(img_static,t,flags.rigidRegister.interpolation,'XData',bounds(:,1)','YData',bounds(:,2)','XYScale',1);
    
    
    for n = N_start+1:N_end
        
        % Retrieve file name
        file = find_name(fileIn, n, lenNum);
        
        % Read current image
        imgIn = im2double(imread(file));
        imgIn = imgIn.*2^(imgBits-numBits);
        
        % Apply space filtering
        if (flags.perform.spaceFilter)
            imgSP = space_filtering(imgIn);
        else
            imgSP = imgIn;
        end
        
        % Create rigidly registered images
        % img_moving_reg = zeros(imgHeight+max_cor(1), imgWidth+max_cor(2), imgDepth);
        % img_moving_reg(1+cor_table(n-N_start+1,3):imgHeight+cor_table(n-N_start+1,3), 1+cor_table(n-N_start+1,4):imgWidth+cor_table(n-N_start+1,4), :) = imgSP;
        t = maketform('affine',[1 0 ; 0 1; cor_table(n-N_start+1,4) cor_table(n-N_start+1,3)]);
        bounds = [1 1; imgWidth+max_cor(2) imgHeight+max_cor(1)];
        img_moving_reg = imtransform(imgSP,t,flags.rigidRegister.interpolation,'XData',bounds(:,1)','YData',bounds(:,2)','XYScale',1);
        
        % Create non-rigidly registered images
        [img_reg, img_grid, O_trans] = bspline_registration(img_static_reg, img_moving_reg, flags.nonRigidRegister.spacing, flags.nonRigidRegister.options, flags.nonRigidRegister.comp, flags.nonRigidRegister.opt_mode);
        O_trans_total{n-N_start+1} = O_trans;
        
        % Set next reference image to currently registered image
        img_static_reg = img_reg;
        
    end
end



% Final processing loop
for n = N_start+1:N_end-1
    
    % Retrieve file name
    file = find_name(fileIn, n, lenNum);

    % Read current image
    imgIn = im2double(imread(file));
    imgIn = imgIn.*2^(imgBits-numBits);
    
    % Apply space filtering
    if (flags.perform.spaceFilter)
        imgSP = space_filtering(imgIn);
    else
        imgSP = imgIn;
    end
    
    % Write image after space filtering
    if (flags.output.spaceFilter)
        dir_out = [fileOut.directory 'spaceFilter/'];
        if (~isdir(dir_out))
            mkdir(dir_out);
        end
        fileOutTemp = fileOut;
        fileOutTemp.directory = dir_out;
        file = find_name(fileOutTemp, n, lenNum);
        imwrite(imgSP./2^(imgBits-numBits) , file);%, 'Compression', 'none');
    end
    
    % Create rigidly registered images
    % img_moving_reg = zeros(imgHeight+max_cor(1), imgWidth+max_cor(2), imgDepth);
    % img_moving_reg(1+cor_table(n-N_start+1,3):imgHeight+cor_table(n-N_start+1,3), 1+cor_table(n-N_start+1,4):imgWidth+cor_table(n-N_start+1,4), :) = imgSP;
    t = maketform('affine',[1 0 ; 0 1; cor_table(n-N_start+1,4) cor_table(n-N_start+1,3)]);
    % bounds = findbounds(t,[1 1; size(I)]);
    % bounds(1,:) = [1 1];
    bounds = [1 1; imgWidth+max_cor(2) imgHeight+max_cor(1)];
    img_moving_reg = imtransform(imgSP,t,flags.rigidRegister.interpolation,'XData',bounds(:,1)','YData',bounds(:,2)','XYScale',1);
    
    % Write image after rigid registration
    if (flags.output.rigidRegister)
        dir_out = [fileOut.directory 'rigidRegister/'];
        if (~isdir(dir_out))
            mkdir(dir_out);
        end
        fileOutTemp = fileOut;
        fileOutTemp.directory = dir_out;
        file = find_name(fileOutTemp, n, lenNum);
        if strcmpi(imgFormat,'uint16')
            imwrite(im2uint16(img_moving_reg./2^(imgBits-numBits)), file);%, 'Compression', 'none');
        else
            imwrite(img_moving_reg./2^(imgBits-numBits), file);%, 'Compression', 'none');
        end
    end
        
    % Create non-rigidly registered images
    if (flags.perform.nonRigidRegister)
        img_reg = bspline_transform(O_trans_total{n-N_start+1}, img_moving_reg, [flags.nonRigidRegister.spacing flags.nonRigidRegister.spacing], 3);
        img_grid = make_grid_image([flags.nonRigidRegister.spacing flags.nonRigidRegister.spacing], [imgHeight+max_cor(1) imgWidth+max_cor(2)]);
        img_grid = bspline_transform(O_trans_total{n-N_start+1}, img_grid, [flags.nonRigidRegister.spacing flags.nonRigidRegister.spacing], 3);
        % img_reg = img_moving;
        % for o = length(O_trans_total):-1:1
        %     img_reg = bspline_transform(O_trans_total{o},img_reg,[spacing spacing],3);
        % end
    else
        img_reg = img_moving_reg;
    end
    
    % Write image after non-rigid registration
    if (flags.output.nonRigidRegister)
        dir_out = [fileOut.directory 'nonRigidRegister/'];
        if (~isdir(dir_out))
            mkdir(dir_out);
        end
        fileOutTemp = fileOut;
        fileOutTemp.directory = dir_out;
        file = find_name(fileOutTemp, n, lenNum);
        imwrite(img_reg./2^(imgBits-numBits), file);%, 'Compression', 'none');
    end
    if (flags.output.nonRigidRegisterGrid)
        dir_out = [fileOut.directory 'nonRigidRegisterGrid/'];
        if (~isdir(dir_out))
            mkdir(dir_out);
        end
        fileOutTemp = fileOut;
        fileOutTemp.directory = dir_out;
        file = find_name(fileOutTemp, n, lenNum);
        imwrite(img_grid, file);%, 'Compression', 'none');
    end
    
    % Apply time filtering
    if (flags.perform.timeFilter)
        
        % Retrieve previous image
        imgTemp = im2double(imread(find_name(fileIn, n-1, lenNum)));
        imgTemp = imgTemp.*2^(imgBits-numBits);
        
        if (flags.perform.spaceFilter)
            imgTemp = space_filtering(imgTemp);
        end
        % imgPrev = zeros(imgHeight+max_cor(1), imgWidth+max_cor(2), imgDepth);
        % imgPrev(1+cor_table(n-N_start+0,3):imgHeight+cor_table(n-N_start+0,3), 1+cor_table(n-N_start+0,4):imgWidth+cor_table(n-N_start+0,4), :) = imgTemp;
        t = maketform('affine',[1 0 ; 0 1; cor_table(n-N_start+0,4) cor_table(n-N_start+0,3)]);
        bounds = [1 1; imgWidth+max_cor(2) imgHeight+max_cor(1)];
        imgPrev = imtransform(imgTemp,t,flags.rigidRegister.interpolation,'XData',bounds(:,1)','YData',bounds(:,2)','XYScale',1);
        if (flags.perform.nonRigidRegister)
            imgPrev = bspline_transform(O_trans_total{n-N_start+0}, imgPrev, [flags.nonRigidRegister.spacing flags.nonRigidRegister.spacing], 3);
        end
        % imgPrev = img_static_reg;
        
        % Retrieve next image
        imgTemp = im2double(imread(find_name(fileIn, n+1, lenNum)));
        imgTemp = imgTemp.*2^(imgBits-numBits);
        
        if (flags.perform.spaceFilter)
            imgTemp = space_filtering(imgTemp);
        end
        % imgNext = zeros(imgHeight+max_cor(1), imgWidth+max_cor(2), imgDepth);
        % imgNext(1+cor_table(n-N_start+2,3):imgHeight+cor_table(n-N_start+2,3), 1+cor_table(n-N_start+2,4):imgWidth+cor_table(n-N_start+2,4), :) = imgTemp;
        t = maketform('affine',[1 0 ; 0 1; cor_table(n-N_start+2,4) cor_table(n-N_start+2,3)]);
        bounds = [1 1; imgWidth+max_cor(2) imgHeight+max_cor(1)];
        imgNext = imtransform(imgTemp,t,flags.rigidRegister.interpolation,'XData',bounds(:,1)','YData',bounds(:,2)','XYScale',1);
        if (flags.perform.nonRigidRegister)
            imgNext = bspline_transform(O_trans_total{n-N_start+2}, imgNext, [flags.nonRigidRegister.spacing flags.nonRigidRegister.spacing], 3);
            % imgNext = bspline_registration(img_reg, imgNext, flags.nonRigidRegister.spacing, flags.nonRigidRegister.options, flags.nonRigidRegister.comp, flags.nonRigidRegister.opt_mode);
        end
        imgTM = time_filtering(imgPrev, img_reg, imgNext);
        
    else
        imgTM = img_reg;
    end
    
    % Write image after time filtering
    if (flags.output.timeFilter)
        dir_out = [fileOut.directory 'timeFilter/'];
        if (~isdir(dir_out))
            mkdir(dir_out);
        end
        fileOutTemp = fileOut;
        fileOutTemp.directory = dir_out;
        file = find_name(fileOutTemp, n, lenNum);
        imwrite(imgTM./2^(imgBits-numBits), file);%, 'Compression', 'none');
    end
    
    % Perform threshold to obtain binary image
    if (imgDepth == 3)
        imgGray = extract_component(imgTM,flags.segment.comp);
    elseif (imgDepth == 1)
        imgGray = imgTM;
    end
    imgBin = im2bw(imgGray, graythresh(imgGray));
    % imgBin = my_kmean(TM,2);
    
    % Write image after thresholding
    if (flags.output.threshold)
        dir_out = [fileOut.directory 'threshold/'];
        if (~isdir(dir_out))
            mkdir(dir_out);
        end
        fileOutTemp = fileOut;
        fileOutTemp.directory = dir_out;
        file = find_name(fileOutTemp, n, lenNum);
        imwrite(imgBin, file);%, 'Compression', 'none');
    end

    % Apply morphological filtering
    [imgMor imgBound] = morphological_filtering(imgBin);
    
    % Write image after morphological filtering
    if (flags.output.morphFilter)
        dir_out = [fileOut.directory 'morphFilter/'];
        if (~isdir(dir_out))
            mkdir(dir_out);
        end
        fileOutTemp = fileOut;
        fileOutTemp.directory = dir_out;
        file = find_name(fileOutTemp, n, lenNum);
        imwrite(imgMor, file);%, 'Compression', 'none');
    end
    
    % Write boundary image
    if (flags.output.boundaries)
        dir_out = [fileOut.directory 'boundaries/'];
        if (~isdir(dir_out))
            mkdir(dir_out);
        end
        fileOutTemp = fileOut;
        fileOutTemp.directory = dir_out;
        file = find_name(fileOutTemp, n, lenNum);
        imwrite(imgBound, file);%, 'Compression', 'none');
    end

    % Apply binary restoration
    t = maketform('affine',[1 0 ; 0 1; cor_table(n-N_start+1,4) cor_table(n-N_start+1,3)]);
    bounds = [1 1; imgWidth+max_cor(2) imgHeight+max_cor(1)];
    imgOut = imtransform(imgIn,t,'XData',bounds(:,1)','YData',bounds(:,2)','XYScale',1);
    % imgOut = zeros(imgHeight+max_cor(1), imgWidth+max_cor(2), imgDepth);
    % imgOut(1+cor_table(n-N_start+1,3):imgHeight+cor_table(n-N_start+1,3), 1+cor_table(n-N_start+1,4):imgWidth+cor_table(n-N_start+1,4), :) = imgIn;
    if (flags.perform.nonRigidRegister)
        imgOut = bspline_transform(O_trans,imgOut,[flags.nonRigidRegister.spacing flags.nonRigidRegister.spacing],3);
    end
    [imgSeg imgSegBound] = binary_restoration(imgOut, imgMor, imgBound);
    
    % Write image after binary restoration
    if (flags.output.binaryMask)
        dir_out = [fileOut.directory 'binaryMask/'];
        if (~isdir(dir_out))
            mkdir(dir_out);
        end
        fileOutTemp = fileOut;
        fileOutTemp.directory = dir_out;
        file = find_name(fileOutTemp, n, lenNum);
        imwrite(imgSegBound, file);%, 'Compression', 'none');
    end

%     name2 = find_name(directory_out, filename, n, ext_out, append);
    
%     OUT = zeros(2*(M+max_cor(1))+1, 2*(N+max_cor(2))+1);
%     OUT(M+max_cor(1)+1,:) = 255;
%     OUT(:,N+max_cor(2)+1) = 255;
% 
%     OUT(1:M+max_cor(1),1:N+max_cor(2))         = IN_big;
%     OUT(1:M+max_cor(1),N+max_cor(2)+2:end)     = 255*double(MOR);
%     OUT(M+max_cor(1)+2:end,1:N+max_cor(2))     = 255*double(BOUND)+SEG;
%     OUT(M+max_cor(1)+2:end,N+max_cor(2)+2:end) = SEG;

    % Write final output image
%     imwrite(uint8(OUT),name2);

end
